/*
 * +=======================================================================+
 * |                                                                       |
 * |          Copyright (C) 2015-16 Nomura Research Institute Ltd          |
 * |                          All Rights Reserved                          |
 * |                                                                       |
 * |    This document is the sole property of Nomura Research Institute    |
 * |    Ltd. No part of this document may be reproduced in any form or     |
 * |    by any means - electronic, mechanical, photocopying, recording     |
 * |    or otherwise - without the prior written permission of Nomura      |
 * |    Research Institute Ltd                                             |
 * |                                                                       |
 * |    Unless required by applicable law or agreed to in writing,         |
 * |    software distributed under the License is distributed on an        |
 * |    "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,       |
 * |    either express or implied.                                         |
 * |                                                                       |
 * +=======================================================================+
 */
package com.nri.trading.oms.ui.main.marketwatch.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ListView;

import com.nri.trading.R;
import com.nri.trading.oms.Constants;
import com.nri.trading.oms.data.local.PreferencesHelper;
import com.nri.trading.oms.data.remote.DataClient;
import com.nri.trading.oms.model.InstrumentQueryResult;
import com.nri.trading.oms.model.InstrumentSummary;
import com.nri.trading.oms.ui.base.BaseActivity;
import com.nri.trading.oms.ui.main.marketwatch.adapter.MarketWatchListAdapter;
import com.nri.trading.oms.ui.main.marketwatch.presenter.MarketWatchFragmentPresenter;
import com.nri.trading.oms.util.InfiniteScrollListener;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;
import me.zhanghai.android.materialprogressbar.MaterialProgressBar;

/**
 * Created by rahulc on 19.1.16.
 */
public class MarketWatchTabFragment extends Fragment implements MarketWatchFragmentView {
    ListView listView;
    Context context;
    String pageTitle;
    @Bind(R.id.market_main_progress)
    MaterialProgressBar progressBar;
    List<InstrumentSummary> instrumentSummaryList = new ArrayList<>();
    SwipeRefreshLayout mSwipeRefreshLayout;
    @Inject
    MarketWatchFragmentPresenter marketWatchFragmentPresenter;
    List<InstrumentSummary> myMarketWatchMainData = new ArrayList<>();
    MarketWatchListAdapter marketWatchListAdapter;
    /*private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;*/


    AutoCompleteTextView searchItem;
    @Inject
    DataClient mDataClient;
    private Handler mHandler;
    ArrayAdapter autoCompleteSearchAdapter;

    List<String> autoSearchContent = new ArrayList<>();

    @SuppressLint("ValidFragment")
    public MarketWatchTabFragment(String pageTitle) {
        this.pageTitle = pageTitle;
        // Required empty public constructor
    }

    public MarketWatchTabFragment() {

    }

    @Override
    public void populateView(InstrumentQueryResult instrumentQueryResult, View rootView) {
        instrumentSummaryList.clear();
        this.instrumentSummaryList.addAll(instrumentQueryResult.getInstrumentList());
        myMarketWatchMainData.addAll(instrumentSummaryList);

        listView = (ListView) rootView.findViewById(R.id.equityOptionList);
        listView.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
        marketWatchListAdapter = new MarketWatchListAdapter(context, myMarketWatchMainData, Integer.parseInt((PreferencesHelper.getSharedPreferenceString(
                context, Constants.PREF_RECORD_PER_PAGE_KEY) == null) ? "5" :
                PreferencesHelper.getSharedPreferenceString(
                        context, Constants.PREF_RECORD_PER_PAGE_KEY)), 1);
        marketWatchListAdapter.notifyDataSetChanged();
        listView.setAdapter(marketWatchListAdapter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View rootView = inflater.inflate(R.layout.inner_fragments, container, false);
        context = container.getContext();
        ((BaseActivity) getActivity()).activityComponent().inject(this);
        ButterKnife.bind(this, rootView);
        marketWatchFragmentPresenter.attachView(this);
        listView = (ListView) rootView.findViewById(R.id.equityOptionList);
        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_refresh_layout);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                myMarketWatchMainData.clear();
                marketWatchListAdapter.notifyDataSetChanged();
                marketWatchFragmentPresenter.listInstruments(rootView, pageTitle, 5, 0);
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });

        searchItem = (AutoCompleteTextView) rootView.findViewById(R.id.searchItem);
        searchItem.setThreshold(Constants.AUTO_COMPLETE_THRESHOLD);


        searchItem.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (searchItem.length() >= Constants.AUTO_COMPLETE_THRESHOLD) {
                    autoCompleteSearchAdapter = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, autoSearchContent);
                    searchItem.setAdapter(autoCompleteSearchAdapter);
                    marketWatchFragmentPresenter.searchUsingSecurityId(1l, "STKOP", searchItem.getText().toString());
                }
                Log.d("Length", String.valueOf(searchItem.getText().length()));
                marketWatchListAdapter.getFilter().filter(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        listView.setOnScrollListener(new InfiniteScrollListener(2) {
            @Override
            public void loadMore(int page, int totalItemsCount) {
                marketWatchFragmentPresenter.listInstruments(rootView, pageTitle, 5, page);
                marketWatchListAdapter.notifyDataSetChanged();

            }
        });


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(getContext(), MarketWatchDetailsActivity.class);
                EventBus.getDefault().postSticky(marketWatchListAdapter.getItem(i));
                getContext().startActivity(intent);
            }
        });
        marketWatchFragmentPresenter.listInstruments(rootView, pageTitle, 5, 0);
        return rootView;

    }

    @Override
    public void populateAutoComplete(InstrumentQueryResult instrumentQueryResult) {
        autoSearchContent = new ArrayList<>();
        for (InstrumentSummary instrumentSummary : instrumentQueryResult.getInstrumentList()) {
            autoSearchContent.add(instrumentSummary.getSecurityCode());
        }
        autoCompleteSearchAdapter.notifyDataSetChanged();
    }

}

