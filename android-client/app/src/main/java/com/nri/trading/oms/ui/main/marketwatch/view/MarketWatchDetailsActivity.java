/*
 * +=======================================================================+
 * |                                                                       |
 * |          Copyright (C) 2015-16 Nomura Research Institute Ltd          |
 * |                          All Rights Reserved                          |
 * |                                                                       |
 * |    This document is the sole property of Nomura Research Institute    |
 * |    Ltd. No part of this document may be reproduced in any form or     |
 * |    by any means - electronic, mechanical, photocopying, recording     |
 * |    or otherwise - without the prior written permission of Nomura      |
 * |    Research Institute Ltd                                             |
 * |                                                                       |
 * |    Unless required by applicable law or agreed to in writing,         |
 * |    software distributed under the License is distributed on an        |
 * |    "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,       |
 * |    either express or implied.                                         |
 * |                                                                       |
 * +=======================================================================+
 */
package com.nri.trading.oms.ui.main.marketwatch.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.appyvet.rangebar.RangeBar;
import com.flipboard.bottomsheet.BottomSheetLayout;
import com.nri.trading.R;
import com.nri.trading.oms.data.model.MarketWatchDetailsData;
import com.nri.trading.oms.data.model.StickViewDetailItem;
import com.nri.trading.oms.model.InstrumentMktDataDTO;
import com.nri.trading.oms.model.InstrumentSummary;
import com.nri.trading.oms.ui.base.BaseActivity;
import com.nri.trading.oms.ui.main.marketwatch.adapter.MarketWatchDetailsAdapter;
import com.nri.trading.oms.ui.main.marketwatch.adapter.StockViewAdapter;
import com.nri.trading.oms.ui.main.marketwatch.presenter.MarketWatchDetailsPresenter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;
import fr.ganfra.materialspinner.MaterialSpinner;

public class MarketWatchDetailsActivity extends BaseActivity implements MarketWatchDetailsView {
    @Inject
    MarketWatchDetailsPresenter marketWatchDetailsPresenter;
    @Bind(R.id.text_md_security_code)
    TextView mdSecurityCode;
    @Bind(R.id.text_md_security_name)
    TextView mdSecurityName;
    @Bind(R.id.text_md_head_value)
    TextView mdHeadValue;
    @Bind(R.id.text_md_rvalue)
    TextView mdLeftValue;
    @Bind(R.id.text_md_lvalue)
    TextView mdRightValue;

    EditText filterMinPrice;
    EditText filterMaxPrice;
    RangeBar filterPriceRangeBar;
    ImageButton mImageButton;
    MaterialSpinner filterMonthSelect;
    RadioGroup filterRadioGroup;

    ListView stockDataListView;
    View stockView;

    BottomSheetLayout bottomSheetLayout;

    Context context;
    InstrumentSummary marketWatchDetailsData = new InstrumentSummary();
    private ListView mRecyclerView;
    private MarketWatchDetailsAdapter marketWatchDetailsListAdapter;

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_market_watch_details);
        Toolbar toolbarEquityOption = (Toolbar) findViewById(R.id.toolbarEquityOption);
        setSupportActionBar(toolbarEquityOption);
        getSupportActionBar().setHomeButtonEnabled(true);
        activityComponent().inject(this);
        ButterKnife.bind(this);

        context = this;
        marketWatchDetailsPresenter.attachView(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        marketWatchDetailsData = EventBus.getDefault().removeStickyEvent(InstrumentSummary.class);
        populateMktDetailsView();

        bottomSheetLayout = (BottomSheetLayout) findViewById(R.id.bottom_sheet_parent_layout);
        stockView = getLayoutInflater().inflate(R.layout.stock_view_bottom_sheet, bottomSheetLayout, false);

        ImageButton stockClose = (ImageButton) stockView.findViewById(R.id.stockClose);
        stockClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetLayout.dismissSheet();
            }
        });
        bottomSheetLayout.setPeekSheetTranslation(950);
        mRecyclerView = (ListView) findViewById(R.id.recycler_stock_view);

        mImageButton = (ImageButton) findViewById(R.id.filter);
        marketWatchDetailsListAdapter = new MarketWatchDetailsAdapter(marketWatchDetailsData.getChildren(), this, bottomSheetLayout);
        mRecyclerView.setAdapter(marketWatchDetailsListAdapter);
        mRecyclerView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                InstrumentSummary instrumentSummary = marketWatchDetailsListAdapter.getItem(i);
                marketWatchDetailsPresenter.getMktDataForSecurity("1", instrumentSummary.getInstrumentPk());
            }

        });

        mImageButton.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                showFilter(v);
                                            }
                                        }
        );


    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_equity_option, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.setting_equity_option) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void showFilter(View v) {
        boolean wrapInScrollView = true;
        View view = getLayoutInflater().inflate(R.layout.layout_filter, null);
        filterMinPrice = (EditText) view.findViewById(R.id.edit_min_price);
        filterMaxPrice = (EditText) view.findViewById(R.id.edit_max_price);
        filterMonthSelect = (MaterialSpinner) view.findViewById(R.id.spinner_month);
        filterPriceRangeBar = (RangeBar) view.findViewById(R.id.range_bar_strike_price);
        filterPriceRangeBar.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
            @Override
            public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex,
                                              int rightPinIndex,
                                              String leftPinValue, String rightPinValue) {
                filterMinPrice.setText(leftPinValue);
                filterMaxPrice.setText(rightPinValue);

            }
        });
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.market_security));
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        filterMonthSelect.setAdapter(arrayAdapter);
        new MaterialDialog.Builder(context)
                .customView(view, wrapInScrollView)
                .show();

        filterRadioGroup = (RadioGroup) view.findViewById(R.id.buy_sell_group_filter);
        filterRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup rGroup, int checkedId) {

                RadioButton checkedRadioButton = (RadioButton) rGroup.findViewById(checkedId);

                boolean isChecked = checkedRadioButton.isChecked();

                if (isChecked && checkedRadioButton.getText().toString().equals("Call")) {
                    marketWatchDetailsListAdapter.registerFilter(MarketWatchDetailsData.class, "eoCall")
                            .setIgnoreCase(true).filter(checkedRadioButton.getText().toString());
                    //MarketWatchDetailsData data = (MarketWatchDetailsData) marketWatchDetailsListAdapter.getItem(5);
                }
                if (isChecked && checkedRadioButton.getText().toString().equals("Put")) {

                    marketWatchDetailsListAdapter.registerFilter(MarketWatchDetailsData.class, "eoCall")
                            .setIgnoreCase(true).filter(checkedRadioButton.getText().toString());
                    //MarketWatchDetailsData data = (MarketWatchDetailsData) marketWatchDetailsListAdapter.getItem(5);

                }
            }
        });

        filterMinPrice.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                marketWatchDetailsListAdapter.registerFilter(MarketWatchDetailsData.class, "eoStrike")
                        .setIgnoreCase(true).filter(s.toString());
            }
        });
    }


    @Override
    public void populateBottomSheet(InstrumentMktDataDTO instrumentMktData) {
        StockViewAdapter stockViewAdapter = new StockViewAdapter(stockView.getContext(), prepareBottomSheetData(instrumentMktData));
        stockDataListView = (ListView) stockView.findViewById(R.id.stockOptionList);
        populateStockView();
        stockDataListView.setAdapter(stockViewAdapter);
        bottomSheetLayout.showWithSheetView(stockView);
    }

    private void populateStockView(){
        TextView svSecurityCode = (TextView)stockView.findViewById(R.id.stockCompanyName);
        TextView svCallPut = (TextView)stockView.findViewById(R.id.text_sv_cal_put);
        TextView svExpireDate = (TextView)stockView.findViewById(R.id.text_sv_expire_date);
        TextView svStrike = (TextView)stockView.findViewById(R.id.text_sv_strike);
        TextView svChange = (TextView)stockView.findViewById(R.id.text_sv_per_change);
        TextView svHeadValue = (TextView)stockView.findViewById(R.id.stockHeadValue);

        svSecurityCode.setText("ToDo");
        svCallPut.setText("ToDo");
        svExpireDate.setText("ToDo");
        svStrike.setText("ToDo");
        svChange.setText("ToDo");
        svHeadValue.setText("ToDo");
    }

    private void populateMktDetailsView(){
        mdSecurityCode.setText(marketWatchDetailsData.getSecurityCode());
        mdSecurityName.setText(marketWatchDetailsData.getSecurityName());

        mdHeadValue.setText(marketWatchDetailsData.getStrikePrice().toString());
        mdLeftValue.setText(marketWatchDetailsData.getBasePrice().toString());
        mdRightValue.setText(marketWatchDetailsData.getLastPrice().toString());

        if (marketWatchDetailsData.getBasePrice() > 0) {
            mdLeftValue.setTextColor(getResources().getColor(R.color.color_positive));
        } else {
            mdLeftValue.setTextColor(getResources().getColor(R.color.color_negative));
        }
        if (marketWatchDetailsData.getLastPrice() > 0) {
            mdRightValue.setTextColor(getResources().getColor(R.color.color_positive));
        } else {
            mdRightValue.setTextColor(getResources().getColor(R.color.color_negative));
        }
    }

    public List<StickViewDetailItem> prepareBottomSheetData(InstrumentMktDataDTO mktData) {
        /*TODO REFACTOR*/
        List<StickViewDetailItem> stickViewDetailItems = new ArrayList<>();
        StickViewDetailItem stickViewDetailItem = new StickViewDetailItem();
        stickViewDetailItem.setLeftValue("64.52");
        stickViewDetailItem.setLeftLabel(getResources().getString(R.string.stock_view_lastPrice));
        stickViewDetailItem.setRightValue("15.25");
        stickViewDetailItem.setRightLabel(getResources().getString(R.string.stock_view_change));
        stickViewDetailItems.add(stickViewDetailItem);
        stickViewDetailItem = new StickViewDetailItem();
        stickViewDetailItem.setLeftValue("08.52");
        stickViewDetailItem.setLeftLabel(getResources().getString(R.string.stock_view_percent));
        stickViewDetailItem.setRightValue(mktData.getOpenPrice().toString());
        stickViewDetailItem.setRightLabel(getResources().getString(R.string.stock_view_openPrice));
        stickViewDetailItems.add(stickViewDetailItem);
        stickViewDetailItem = new StickViewDetailItem();
        stickViewDetailItem.setLeftValue(mktData.getLowPrice().toString());
        stickViewDetailItem.setLeftLabel(getResources().getString(R.string.stock_view_lowPrice));
        stickViewDetailItem.setRightValue(mktData.getHighPrice().toString());
        stickViewDetailItem.setRightLabel(getResources().getString(R.string.stock_view_highPrice));
        stickViewDetailItems.add(stickViewDetailItem);
        stickViewDetailItem = new StickViewDetailItem();
        stickViewDetailItem.setLeftValue(mktData.getBidPrice().toString());
        stickViewDetailItem.setLeftLabel(getResources().getString(R.string.stock_view_bidPrice));
        stickViewDetailItem.setRightValue(mktData.getAskPrice().toString());
        stickViewDetailItem.setRightLabel(getResources().getString(R.string.stock_view_askPrice));
        stickViewDetailItems.add(stickViewDetailItem);
        stickViewDetailItem = new StickViewDetailItem();
        stickViewDetailItem.setLeftValue(mktData.getNoOfBids().toString());
        stickViewDetailItem.setLeftLabel(getResources().getString(R.string.stock_view_noOfBids));
        stickViewDetailItem.setRightValue(mktData.getNoOfAsks().toString());
        stickViewDetailItem.setRightLabel(getResources().getString(R.string.stock_view_noOfAsks));
        stickViewDetailItems.add(stickViewDetailItem);
        stickViewDetailItem = new StickViewDetailItem();
        stickViewDetailItem.setLeftValue(mktData.getVolume().toString());
        stickViewDetailItem.setLeftLabel(getResources().getString(R.string.stock_view_volume));
        stickViewDetailItem.setRightValue(mktData.getValue().toString());
        stickViewDetailItem.setRightLabel(getResources().getString(R.string.stock_view_value));
        stickViewDetailItems.add(stickViewDetailItem);

        return stickViewDetailItems;
    }
}
