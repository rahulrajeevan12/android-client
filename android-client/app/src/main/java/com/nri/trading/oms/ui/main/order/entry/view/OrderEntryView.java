package com.nri.trading.oms.ui.main.order.entry.view;

import com.nri.trading.oms.model.ValidationResponse;
import com.nri.trading.oms.ui.base.MvpView;

import retrofit.RetrofitError;

/**
 * Created by deor on 05-02-2016.
 */
public interface OrderEntryView extends MvpView {

    void populateOrderEntry() ;
    void onValidateSuccess(boolean status, ValidationResponse response);

    void onValidateFailure(RetrofitError error);
}
