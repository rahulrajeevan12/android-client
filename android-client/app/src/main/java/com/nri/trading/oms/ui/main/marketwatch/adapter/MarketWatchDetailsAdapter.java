package com.nri.trading.oms.ui.main.marketwatch.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.flipboard.bottomsheet.BottomSheetLayout;
import com.nri.trading.R;
import com.nri.trading.oms.data.model.StickViewDetailItem;
import com.nri.trading.oms.model.InstrumentSummary;
import com.nri.trading.oms.ui.base.Adapter.SearchAdapter;
import com.nri.trading.oms.ui.main.order.entry.view.OrderEntryActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by deor on 29-01-2016.
 */
public class MarketWatchDetailsAdapter extends SearchAdapter<InstrumentSummary> {
    BottomSheetLayout bottomSheetLayout;
    List<InstrumentSummary> mktDataList = new ArrayList<>();

    class ViewHolder {

        @Bind(R.id.text_security_name)
        TextView securityName;
        @Bind(R.id.eoHeadValue)
        TextView eoHeadValue;
        @Bind(R.id.eoRvalue)
        TextView eoRValue;
        @Bind(R.id.eoLvalue)
        TextView eoLValue;
        @Bind(R.id.eoExpire)
        TextView eoExpire;
        @Bind(R.id.eoStrike)
        TextView eoStrike;
        @Bind(R.id.eoCall)
        TextView eoCall;
        @Bind(R.id.action_type_popup)
        ImageView action_type;


        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    public MarketWatchDetailsAdapter(List<InstrumentSummary> marketWatchDetailsData, Context context, BottomSheetLayout bottomSheetLayout) {
        super(marketWatchDetailsData, context);
        this.bottomSheetLayout = bottomSheetLayout;
        this.mktDataList.addAll(marketWatchDetailsData);
    }


    public void setStockValue(ViewHolder holder) {

        View parentView = layoutInflater.inflate(R.layout.bottom_sheet_layout, null);
        TextView shortName = (TextView) parentView.findViewById(R.id.text_md_security_code);
        String ShortName = (String) shortName.getText();
        TextView headValue1, headValue2, headValue3, headValue4, headValue5, headValue6;
        ListView listView;
        final View stockView = layoutInflater.inflate(R.layout.stock_view_bottom_sheet, bottomSheetLayout, false);
        ImageButton stockClose = (ImageButton) stockView.findViewById(R.id.stockClose);
        stockClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetLayout.dismissSheet();
            }
        });
        headValue1 = (TextView) stockView.findViewById(R.id.stockCompanyName);
        headValue2 = (TextView) stockView.findViewById(R.id.stockHeadValue);
        headValue3 = (TextView) stockView.findViewById(R.id.text_sv_cal_put);
        headValue4 = (TextView) stockView.findViewById(R.id.text_sv_per_change);
        headValue5 = (TextView) stockView.findViewById(R.id.text_sv_expire_date);
        headValue6 = (TextView) stockView.findViewById(R.id.text_sv_strike);
        listView = (ListView) stockView.findViewById(R.id.stockOptionList);

        headValue1.setText("(" + ShortName + ")" + holder.securityName.getText());
        headValue2.setText(holder.eoHeadValue.getText());
        headValue3.setText(holder.eoCall.getText());
        String lvalue = (String) holder.eoLValue.getText();
        lvalue = lvalue.replace("%", "");
        headValue4.setText(lvalue);
        headValue5.setText(holder.eoExpire.getText());
        headValue6.setText(holder.eoStrike.getText());
        StockViewAdapter stockViewAdapter = new StockViewAdapter(stockView.getContext(), getData());
        listView.setAdapter(stockViewAdapter);
        bottomSheetLayout.showWithSheetView(stockView);

    }

    public static List<StickViewDetailItem> getData() {
        List<StickViewDetailItem> stickViewDetailItems = new ArrayList<>();
        StickViewDetailItem stickViewDetailItem = new StickViewDetailItem();
        stickViewDetailItem.setLeftLabel("Last Price");
        stickViewDetailItem.setLeftValue("12.52");
        stickViewDetailItem.setRightLabel("Bid");
        stickViewDetailItem.setRightValue("15.25");
        for (int index = 1; index <= 5; index++)
            stickViewDetailItems.add(stickViewDetailItem);

        return stickViewDetailItems;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.market_watch_details_list, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        final InstrumentSummary currentCompanyHistoryData = mktDataList.get(position);
        viewHolder.securityName.setText(currentCompanyHistoryData.getSecurityName());
        //TODO remove hardcode values
        if (currentCompanyHistoryData.getLastPrice() == null)
            currentCompanyHistoryData.setLastPrice(50.24d);
        viewHolder.eoHeadValue.setText(currentCompanyHistoryData.getLastPrice().toString());
        viewHolder.eoExpire.setText(currentCompanyHistoryData.getExpiryDate());
        if (currentCompanyHistoryData.getStrikePrice() == null)
            currentCompanyHistoryData.setStrikePrice(50.24d);
        viewHolder.eoStrike.setText(currentCompanyHistoryData.getStrikePrice().toString());
        viewHolder.eoCall.setText(currentCompanyHistoryData.getOptionType());

        final ImageView action_type = viewHolder.action_type;
        final int location = position;
        viewHolder.action_type.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Creating the instance of PopupMenu
                PopupMenu popup = new PopupMenu(context, action_type);

                SubMenu sb1 = popup.getMenu().addSubMenu("Buy");
                SubMenu sb2 = popup.getMenu().addSubMenu("Sell");
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        if (item.toString().equals("Buy")) {
                            Intent orderEntryIntent = new Intent(context, OrderEntryActivity.class);
                            orderEntryIntent.putExtra("BuySellType", "Buy");
                            de.greenrobot.event.EventBus.getDefault().postSticky(mktDataList.get(location));
                            context.startActivity(orderEntryIntent);


                        }


                        // some code here
                        if (item.toString().equals("Sell")) {
                            Intent orderEntryIntent = new Intent(context, OrderEntryActivity.class);
                            orderEntryIntent.putExtra("BuySellType", "Sell");
                            de.greenrobot.event.EventBus.getDefault().postSticky(mktDataList.get(location));
                            context.startActivity(orderEntryIntent);
                        }


                        return true;
                    }
                });

                popup.show();//showing popup menu


            }

        });
       /* convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setStockValue(viewHolder);
            }
        });*/
        return convertView;
    }

    @Override
    public InstrumentSummary getItem(int i) {
        return mktDataList.get(i);
    }


}
