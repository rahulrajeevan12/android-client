package com.nri.trading.oms.ui.main.order.entry.view;

import com.nri.trading.oms.model.OrderEntryRequest;
import com.nri.trading.oms.ui.base.MvpView;

import retrofit.RetrofitError;

/**
 * Created by deor on 05-02-2016.
 */
public interface OrderConfirmationView extends MvpView{

    void onSubmitSuccess(boolean status, OrderEntryRequest response);

    void onSubmitFailure(RetrofitError error);


}
