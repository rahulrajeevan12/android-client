/*
 * +=======================================================================+
 * |                                                                       |
 * |          Copyright (C) 2015-16 Nomura Research Institute Ltd          |
 * |                          All Rights Reserved                          |
 * |                                                                       |
 * |    This document is the sole property of Nomura Research Institute    |
 * |    Ltd. No part of this document may be reproduced in any form or     |
 * |    by any means - electronic, mechanical, photocopying, recording     |
 * |    or otherwise - without the prior written permission of Nomura      |
 * |    Research Institute Ltd                                             |
 * |                                                                       |
 * |    Unless required by applicable law or agreed to in writing,         |
 * |    software distributed under the License is distributed on an        |
 * |    "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,       |
 * |    either express or implied.                                         |
 * |                                                                       |
 * +=======================================================================+
 */
package com.nri.trading.oms.ui.main.marketwatch.presenter;

import android.util.Log;
import android.view.View;

import com.nri.trading.oms.api.RefdatacontrollerApi;
import com.nri.trading.oms.data.remote.DataClient;
import com.nri.trading.oms.model.InstrumentQueryCriteria;
import com.nri.trading.oms.model.InstrumentQueryResult;
import com.nri.trading.oms.model.InstrumentSummary;
import com.nri.trading.oms.ui.base.Presenter;
import com.nri.trading.oms.ui.main.marketwatch.view.MarketWatchFragmentView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by rahulc on 25.1.16.
 */

public class MarketWatchFragmentPresenter implements Presenter<MarketWatchFragmentView> {
    MarketWatchFragmentView marketWatchFragmentView;

    @Inject
    DataClient mDataClient;

    @Inject
    public MarketWatchFragmentPresenter() {
    }

    @Override
    public void attachView(MarketWatchFragmentView mvpView) {
        this.marketWatchFragmentView = mvpView;
    }

    @Override
    public void detachView() {
        this.marketWatchFragmentView = null;
    }

    /* public List<InstrumentSummary> getMarketData() {
     }
 */
    public void listInstruments(final View rootView, final String securityType ,int pageSize ,int startIndex ) {
        /* TODO To be removed*/
        if(true) {
            InstrumentQueryResult instrumentQueryResult = new InstrumentQueryResult();
            List<InstrumentSummary> instrumentList = new ArrayList();
            List<InstrumentSummary> instrumentLisC = new ArrayList();
            Double dbl = 631.0;
            Double dbl1 = 923.0;
            InstrumentSummary instrumentSummary = new InstrumentSummary();
            if(startIndex == 1){
            instrumentSummary.setBasePrice(dbl);
            instrumentSummary.setExpiryDate("15/11/1988");
            instrumentSummary.setInstrumentPk("12");
            instrumentSummary.setOptionType("CAL");
            instrumentSummary.setStrikePrice(dbl);
            instrumentSummary.setLastPrice(dbl);
            instrumentSummary.setSecurityCode("SBI");
            instrumentSummary.setSecurityName("State Bank of India PVT State Bank of India PVT");
            instrumentSummary.setLastPrice(dbl1);
            }
            else{

                instrumentSummary.setBasePrice(dbl);
                instrumentSummary.setStrikePrice(dbl);
                instrumentSummary.setLastPrice(dbl);
                instrumentSummary.setExpiryDate("15/11/1988");
                instrumentSummary.setInstrumentPk("12");
                instrumentSummary.setOptionType("CAL");
                instrumentSummary.setSecurityCode("UBI");
                instrumentSummary.setSecurityName("Union Bank Of India");
                instrumentSummary.setLastPrice(dbl1);
            }
            for (int i = 0; i <= 10; i++) {
                instrumentLisC.add(instrumentSummary);
            }
            instrumentSummary.setChildren(instrumentLisC);
            for (int i = 0; i <= 10; i++) {
                instrumentList.add(instrumentSummary);
            }
            instrumentQueryResult.setInstrumentList(instrumentList);
            instrumentQueryResult.setCount(instrumentList.size());
            marketWatchFragmentView.populateView(instrumentQueryResult, rootView);
            return;
        }
        /*To be removed*/
        final Map<String, String> securityTypeMap = new HashMap<>();
        securityTypeMap.put("Equity Option", "STKOP");
        securityTypeMap.put("Index Future", "INXFU");
        securityTypeMap.put("Index Option", "INXOP");
        RefdatacontrollerApi refdatacontrollerApi = mDataClient.createService(RefdatacontrollerApi.class);
        InstrumentQueryCriteria instrumentQueryCriteria = new InstrumentQueryCriteria();
        instrumentQueryCriteria.setMarketPk(1l);
        instrumentQueryCriteria.setPageSize(pageSize);
        instrumentQueryCriteria.setStartIndex(startIndex);
        instrumentQueryCriteria.setSecurityType(securityTypeMap.get(securityType));
        refdatacontrollerApi.listInstrumentUsingPOST(instrumentQueryCriteria, new Callback<InstrumentQueryResult>() {

            @Override
            public void success(InstrumentQueryResult instrumentQueryResult, Response response) {
                marketWatchFragmentView.populateView(instrumentQueryResult, rootView);
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d("Instrument List Errors", error.toString());

            }
        });


    }

    public void searchUsingSecurityId(Long marketPk, String instrumentType, String securityIDPrefix) {
        RefdatacontrollerApi refdatacontrollerApi = mDataClient.createService(RefdatacontrollerApi.class);
        refdatacontrollerApi.securityIDSearchUsingGET(marketPk, instrumentType, securityIDPrefix, new Callback<InstrumentQueryResult>() {
            @Override
            public void success(InstrumentQueryResult instrumentQueryResult, Response response) {
                marketWatchFragmentView.populateAutoComplete(instrumentQueryResult);

            }

            @Override
            public void failure(RetrofitError error) {
                Log.d("Error", error.toString());
            }
        });

    }
}