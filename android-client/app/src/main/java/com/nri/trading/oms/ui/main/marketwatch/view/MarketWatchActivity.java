/*
 * +=======================================================================+
 * |                                                                       |
 * |          Copyright (C) 2015-16 Nomura Research Institute Ltd          |
 * |                          All Rights Reserved                          |
 * |                                                                       |
 * |    This document is the sole property of Nomura Research Institute    |
 * |    Ltd. No part of this document may be reproduced in any form or     |
 * |    by any means - electronic, mechanical, photocopying, recording     |
 * |    or otherwise - without the prior written permission of Nomura      |
 * |    Research Institute Ltd                                             |
 * |                                                                       |
 * |    Unless required by applicable law or agreed to in writing,         |
 * |    software distributed under the License is distributed on an        |
 * |    "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,       |
 * |    either express or implied.                                         |
 * |                                                                       |
 * +=======================================================================+
 */
package com.nri.trading.oms.ui.main.marketwatch.view;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.nri.trading.R;
import com.nri.trading.oms.data.local.PreferencesHelper;
import com.nri.trading.oms.model.InlineResponse200;
import com.nri.trading.oms.ui.main.marketwatch.adapter.MarketWatchPagerAdapter;
import com.nri.trading.oms.ui.main.marketwatch.presenter.MarketWatchPresenter;
import com.nri.trading.oms.ui.main.navigation.view.NavigationActivity;
import com.nri.trading.oms.util.NetworkUtil;

import java.util.ArrayList;
import java.util.Map;

import javax.inject.Inject;

import butterknife.ButterKnife;
import fr.ganfra.materialspinner.MaterialSpinner;

public class MarketWatchActivity extends NavigationActivity implements MarketWatchView {
    private MarketWatchPagerAdapter mMarketWatchPagerAdapter;


    @Inject
    MarketWatchPresenter marketWatchPresenter;


    private ViewPager mViewPager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        DrawerLayout mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        activityComponent().inject(this);
        ButterKnife.bind(this);


        marketWatchPresenter.attachView(this);
        if (NetworkUtil.isNetworkConnected(getApplicationContext()))
            marketWatchPresenter.getMarket();
        else
            Toast.makeText(this,R.string.no_network_connection,Toast.LENGTH_LONG);

        //Inflate your activity layout
        View contentView = inflater.inflate(R.layout.activity_market_watch, null, false);
        mDrawer.addView(contentView, 0);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_market_watch);
        setSupportActionBar(toolbar);

        mMarketWatchPagerAdapter = new MarketWatchPagerAdapter(getSupportFragmentManager(),this);
        mViewPager = (ViewPager) findViewById(R.id.pager_market_watch);
        mViewPager.setAdapter(mMarketWatchPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_market_watch);
        tabLayout.setupWithViewPager(mViewPager);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        /*To be removed*/
        MaterialSpinner spinner = (MaterialSpinner) findViewById(R.id.spinner_market);
        ArrayList<String> marketList = new ArrayList<>();
        marketList.add("ASX");
        marketList.add("BSX");
        marketList.add("CSX");
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, R.layout.spinner_layout, marketList);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(arrayAdapter);
        /*To be removed*/

    }


    @Override
    public void populateMarketSpinner(InlineResponse200 inlineResponse200) {
        ArrayList<String> marketList = new ArrayList<>();
        for (Map.Entry<String, Object> entry : inlineResponse200.entrySet()) {
            PreferencesHelper.setSharedPreferenceString(this, entry.getValue().toString(), entry.getKey().toString());
            marketList.add(entry.getValue().toString());
        }
        MaterialSpinner spinner = (MaterialSpinner) findViewById(R.id.spinner_market);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, R.layout.spinner_layout, marketList);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(arrayAdapter);
    }
}
