package com.nri.trading.oms.ui.main.settings.presenter;

import android.util.Log;

import com.nri.trading.oms.api.RefdatacontrollerApi;
import com.nri.trading.oms.data.remote.DataClient;
import com.nri.trading.oms.model.InlineResponse200;
import com.nri.trading.oms.ui.base.Presenter;
import com.nri.trading.oms.ui.main.settings.view.SettingsView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by rahulc on 3.2.16.
 */
public class SettingsPresenter implements Presenter<SettingsView> {
    SettingsView settingsView;

    @Inject
    DataClient mDataClient;

    @Inject
    public SettingsPresenter() {

    }

    @Override
    public void attachView(SettingsView mvpView) {
        this.settingsView = mvpView;
    }

    @Override
    public void detachView() {
        this.settingsView = null;
    }

    public void getPrefConstraints(){
        RefdatacontrollerApi refService = mDataClient.createService(RefdatacontrollerApi.class);
        List<String> requestBody = new ArrayList<>();
        requestBody.add("LOCALE");
        requestBody.add("RECORDS_PER_PAGE");
        refService.getConstraintsUsingPOST(requestBody, new Callback<InlineResponse200>() {
            @Override
            public void success(InlineResponse200 inlineResponse200, Response response) {
                Log.d("Success",inlineResponse200.toString());
                settingsView.populatePrefData(inlineResponse200);
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });

    }


}
