package com.nri.trading.oms.ui.main.settings.view;

import com.nri.trading.oms.model.InlineResponse200;
import com.nri.trading.oms.ui.base.MvpView;

/**
 * Created by rahulc on 3.2.16.
 */
public interface SettingsView extends MvpView {

    void populatePrefData(InlineResponse200 inlineResponse200);

}
