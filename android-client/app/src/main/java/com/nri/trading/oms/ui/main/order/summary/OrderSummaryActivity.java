package com.nri.trading.oms.ui.main.order.summary;

import android.os.Bundle;

import com.nri.trading.R;
import com.nri.trading.oms.ui.base.BaseActivity;

/**
 * Created by deor on 18-01-2016.
 */
public class OrderSummaryActivity extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_summary);
    }
}
