/*
 * +=======================================================================+
 * |                                                                       |
 * |          Copyright (C) 2015-16 Nomura Research Institute Ltd          |
 * |                          All Rights Reserved                          |
 * |                                                                       |
 * |    This document is the sole property of Nomura Research Institute    |
 * |    Ltd. No part of this document may be reproduced in any form or     |
 * |    by any means - electronic, mechanical, photocopying, recording     |
 * |    or otherwise - without the prior written permission of Nomura      |
 * |    Research Institute Ltd                                             |
 * |                                                                       |
 * |    Unless required by applicable law or agreed to in writing,         |
 * |    software distributed under the License is distributed on an        |
 * |    "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,       |
 * |    either express or implied.                                         |
 * |                                                                       |
 * +=======================================================================+
 */

package com.nri.trading.oms.ui.main.navigation.view;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;

import com.nri.trading.R;
import com.nri.trading.oms.Constants;
import com.nri.trading.oms.data.local.PreferencesHelper;
import com.nri.trading.oms.data.remote.DataClient;
import com.nri.trading.oms.model.MenuDTO;
import com.nri.trading.oms.ui.base.BaseActivity;
import com.nri.trading.oms.ui.main.login.view.LoginActivity;
import com.nri.trading.oms.ui.main.navigation.adapter.CustomExpandableListAdapter;
import com.nri.trading.oms.ui.main.navigation.presenter.NavigationPresenter;
import com.nri.trading.oms.ui.main.order.entry.view.OrderEntryActivity;
import com.nri.trading.oms.ui.main.settings.view.SettingsActivity;
import com.nri.trading.oms.util.DialogFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.inject.Inject;

/**
 * Created by deor on 12-01-2016.
 */
public class NavigationActivity extends BaseActivity implements NavigationView {

    @Inject
    DataClient mDataClient;


    private DrawerLayout mDrawerLayout;
    private Context context;

    private ExpandableListView mExpandableListView;

    private ActionBarDrawerToggle mDrawerToggle;
    private String mActivityTitle;
    private ExpandableListAdapter mExpandableListAdapter;
    private List<String> mExpandableListTitle;
    private Map<String, List<String>> mExpandableListData;

    private Map<String, String> activityMap;

    private int lastExpandedPosition = -1;

    @Inject
    NavigationPresenter navigationPresenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);
        activityComponent().inject(this);
        navigationPresenter.attachView(this);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mExpandableListView = (ExpandableListView) findViewById(R.id.expandableListView);

        mActivityTitle = getTitle().toString();
        LayoutInflater inflater = getLayoutInflater();
        View listHeaderView = inflater.inflate(R.layout.nav_header, null, false);
        mExpandableListView.addHeaderView(listHeaderView);

        setupDrawer();
        context = this;
        navigationPresenter.getMenuList();
    }

    private void addDrawerItems() {
        mExpandableListAdapter = new CustomExpandableListAdapter(this, mExpandableListTitle, mExpandableListData);
        mExpandableListView.setAdapter(mExpandableListAdapter);
        mExpandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                getSupportActionBar().setTitle(mExpandableListTitle.get(groupPosition).toString());
                if (lastExpandedPosition != -1
                        && groupPosition != lastExpandedPosition) {
                    mExpandableListView.collapseGroup(lastExpandedPosition);
                }
                lastExpandedPosition = groupPosition;
            }
        });


        mExpandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {

            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {

                String selectedItem = mExpandableListTitle.get(groupPosition).toString();
                if (((List) (mExpandableListData.get(mExpandableListTitle.get(groupPosition)))).size() == 0) {
                    switch (groupPosition) {
                        case 0:
                            mDrawerLayout.closeDrawer(GravityCompat.START);
                            break;
                        case 3:
                            Intent settingsIntent = new Intent(NavigationActivity.this, SettingsActivity.class);
                            startActivity(settingsIntent);
                            break;
                        case 4:
                            DialogFactory.createConfirmDialog(context, R.string.confirm_logout_title, R.string.confirm_logout_description,
                                    R.string.dialog_action_cancel, R.string.dialog_action_ok, doLogout(), logoutCancel());

                    }
                }
                return false;
            }

        });
        mExpandableListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
            @Override
            public void onGroupCollapse(int groupPosition) {
                // getSupportActionBar().setTitle(R.string.navigation);
                // mSelectedItemView.setText(R.string.selected_item);
            }
        });
        mExpandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                String selectedItem = ((List) (mExpandableListData.get(mExpandableListTitle.get(groupPosition))))
                        .get(childPosition).toString();

                switch (selectedItem) {
                    case "trading.oms.menu.orderentry":
                        Intent orderEntryIntent = new Intent(NavigationActivity.this, OrderEntryActivity.class);
                        startActivity(orderEntryIntent);

                }
                mDrawerLayout.closeDrawer(GravityCompat.START);
                return false;
            }
        });
    }

    private void setupDrawer() {
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.drawer_open, R.string.drawer_close) {

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                //getSupportActionBar().setTitle(R.string.navigation);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                getSupportActionBar().setTitle(mActivityTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };

        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration config) {
        super.onConfigurationChanged(config);
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());
        setContentView(R.layout.activity_navigation);
        mDrawerToggle.onConfigurationChanged(config);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Activate the navigation drawer toggle
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        mDrawerLayout.closeDrawers();
    }

    private void restartActivity() {
        Intent intent = getIntent();
        finish();
        startActivity(intent);
    }

    @Override
    public void populateMenu(List<MenuDTO> menuList) {
        MenuDTO staticItem = new MenuDTO();
        staticItem.setNameKey("trading.oms.static.menu.settings");
        menuList.add(staticItem);
        staticItem = new MenuDTO();
        staticItem.setNameKey("trading.oms.static.menu.logout");
        menuList.add(staticItem);

        mExpandableListData = new LinkedHashMap<>();
        activityMap = new HashMap<>();
        List<MenuDTO> menuDTOs = new ArrayList<>();
        menuDTOs.addAll(menuList);
        for (MenuDTO menuDTO : menuList) {
            String resourceName = menuDTO.getNameKey();
            int resourceId = getResources().getIdentifier(resourceName, "string", getPackageName());
            if (menuDTO.getChildren() == null) {
                mExpandableListData.put(getResources().getString(resourceId), new ArrayList<String>());
                //TODO url mapping
                activityMap.put(getResources().getString(resourceId), menuDTO.getUrl());
                // if(menuDTO.getStyleClass() == null)
            } else {
                List<String> subMenus = new ArrayList<>();
                for (MenuDTO childMenu : menuDTO.getChildren()) {
                    subMenus.add(childMenu.getNameKey());
                    subMenus.add(childMenu.getNameKey());
                }
                mExpandableListData.put(getResources().getString(resourceId), subMenus);
            }
        }
        mExpandableListTitle = new ArrayList(mExpandableListData.keySet());
        addDrawerItems();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        navigationPresenter.detachView();
    }


    public Runnable doLogout() {
        return new Runnable() {
            public void run() {
                PreferencesHelper.removePreferenceString(getApplicationContext(), Constants.JWT_TOKEN_KEY);
                Intent loginIntent = new Intent(NavigationActivity.this, LoginActivity.class);
                startActivity(loginIntent);
                finish();
            }
        };
    }

    public Runnable logoutCancel() {
        return new Runnable() {
            public void run() {
                mDrawerLayout.closeDrawer(GravityCompat.START);
            }
        };
    }
}
