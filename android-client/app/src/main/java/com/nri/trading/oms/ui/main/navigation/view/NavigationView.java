package com.nri.trading.oms.ui.main.navigation.view;

import com.nri.trading.oms.model.MenuDTO;
import com.nri.trading.oms.ui.base.MvpView;

import java.util.List;

/**
 * Created by deor on 12-01-2016.
 */
public interface NavigationView extends MvpView {
    void populateMenu(List<MenuDTO> menuList);
}
