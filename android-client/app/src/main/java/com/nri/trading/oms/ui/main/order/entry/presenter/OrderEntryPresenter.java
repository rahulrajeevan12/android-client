package com.nri.trading.oms.ui.main.order.entry.presenter;

import android.util.Log;

import com.nri.trading.oms.api.OrdercontrollerApi;
import com.nri.trading.oms.data.remote.DataClient;
import com.nri.trading.oms.model.OrderEntryRequest;
import com.nri.trading.oms.model.ValidationResponse;
import com.nri.trading.oms.ui.base.Presenter;
import com.nri.trading.oms.ui.main.order.entry.view.OrderEntryView;

import javax.inject.Inject;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by deor on 05-02-2016.
 */
public class OrderEntryPresenter implements Presenter<OrderEntryView> {
    OrderEntryView orderEntryView;
    @Inject
    DataClient mDataClient;
    @Inject
    public OrderEntryPresenter(){

    }
    public void doValidate(OrderEntryRequest orderEntryRequest){
        if(true){
            ValidationResponse validationResponse  = new ValidationResponse();
            orderEntryView.onValidateSuccess(true,validationResponse);
            return;
        }
        OrdercontrollerApi ordercontrollerApi = mDataClient.createService(OrdercontrollerApi.class);
        ordercontrollerApi.validateEntryUsingPOST(orderEntryRequest, new Callback<ValidationResponse>() {
            @Override
            public void success(ValidationResponse validationResponse, Response response) {
                orderEntryView.onValidateSuccess(true,validationResponse);
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d("Validation Error", error.toString());
                orderEntryView.onValidateFailure(error);
            }
        });
    }

    @Override
    public void attachView(OrderEntryView orderEntryView) {
        this.orderEntryView = orderEntryView;
    }

    @Override
    public void detachView() {
        this.orderEntryView = null;
    }
}
