/*
 * +=======================================================================+
 * |                                                                       |
 * |          Copyright (C) 2015-16 Nomura Research Institute Ltd          |
 * |                          All Rights Reserved                          |
 * |                                                                       |
 * |    This document is the sole property of Nomura Research Institute    |
 * |    Ltd. No part of this document may be reproduced in any form or     |
 * |    by any means - electronic, mechanical, photocopying, recording     |
 * |    or otherwise - without the prior written permission of Nomura      |
 * |    Research Institute Ltd                                             |
 * |                                                                       |
 * |    Unless required by applicable law or agreed to in writing,         |
 * |    software distributed under the License is distributed on an        |
 * |    "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,       |
 * |    either express or implied.                                         |
 * |                                                                       |
 * +=======================================================================+
 */

package com.nri.trading.oms.ui.main.login.view;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.nri.trading.R;
import com.nri.trading.oms.Constants;
import com.nri.trading.oms.data.local.PreferencesHelper;
import com.nri.trading.oms.ui.base.BaseActivity;
import com.nri.trading.oms.ui.main.login.presenter.LoginPresenter;
import com.nri.trading.oms.ui.main.marketwatch.view.MarketWatchActivity;
import com.nri.trading.oms.util.DialogFactory;
import com.nri.trading.oms.util.NetworkUtil;
import com.nri.trading.oms.util.ViewUtil;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.RetrofitError;


public class LoginActivity extends BaseActivity implements LoginView {

    @Inject
    LoginPresenter mLoginPresenter;

    // UI references.
    @Bind(R.id.edit_user_name)
    EditText mUserName;
    @Bind(R.id.edit_password)
    EditText mPassword;
    @Bind(R.id.button_login)
    Button mLoginButton;
    Context context;
    private ProgressDialog progressDialog;


    private static final int CREDENTIALS_MIN_LENGTH = 3;

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        setContentView(R.layout.activity_login);
        activityComponent().inject(this);
        ButterKnife.bind(this);
        mLoginPresenter.attachView(this);
        mLoginButton.setEnabled(false);
        mLoginButton.setTextColor(getResources().getColor(R.color.color_button_inactive_font));

        /*Adds a TextWatcher listener to listen text changes*/
        mUserName.addTextChangedListener(mLoginInputWatcher);
        mPassword.addTextChangedListener(mLoginInputWatcher);
        //DialogFactory.createProgressDialog(this, "Invalid credentials").show();

        /*Handle the IME_ACTION of the keyboard*/
        mPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.ime_actionid_login || id == EditorInfo.IME_NULL) {
                    if (mUserName.getText().toString().length() >= CREDENTIALS_MIN_LENGTH && mPassword.getText().toString().length() >= CREDENTIALS_MIN_LENGTH) {
                        attemptLogin();
                        return true;
                    }

                }
                return false;
            }
        });

        mLoginButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                    attemptLogin();
            }
        });


    }


    private void attemptLogin() {
        ViewUtil.hideKeyboard(LoginActivity.this);
        if (NetworkUtil.isNetworkConnected(getApplicationContext())){
            String userName = mUserName.getText().toString();
            String password = mPassword.getText().toString();
            progressDialog = DialogFactory.createProgressDialog(this, R.string.progress_logging_in);
            progressDialog.show();
            mLoginPresenter.doLogin(userName, password);
        }
        else
            Snackbar.make(mLoginButton, R.string.no_network_connection, Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
    }

    private TextWatcher mLoginInputWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override
        public void afterTextChanged(Editable editable) {

            if (mUserName.getText().toString().length() >= CREDENTIALS_MIN_LENGTH && mPassword.getText().toString().length() >= CREDENTIALS_MIN_LENGTH) {
                mLoginButton.setEnabled(true);
                mLoginButton.setTextColor(getResources().getColor(R.color.color_font_primary));
            } else {
                mLoginButton.setEnabled(false);
                mLoginButton.setTextColor(getResources().getColor(R.color.color_button_inactive_font));
            }
        }
    };

    @Override
    public void onLoginSuccess(boolean status, String response) {
        if (status == true) {
            PreferencesHelper.setSharedPreferenceString(context, Constants.JWT_TOKEN_KEY, response);
            PreferencesHelper.setSharedPreferenceString(context, Constants.USER_LOGGED_IN_KEY, Constants.TRUE);
            Intent marketWatchActivity = new Intent(getApplicationContext(), MarketWatchActivity.class);
            startActivity(marketWatchActivity);
            finish();
        }
        progressDialog.hide();
    }


    @Override
    public void onLoginFailure(RetrofitError error) {
        progressDialog.hide();
        DialogFactory.createSimpleOkErrorDialog(this, R.string.invalid_credentials_header, R.string.invalid_credentials_body).show();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        mLoginPresenter.detachView();
    }

}

