package com.nri.trading.oms.ui.main.order.entry.view;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.nri.trading.R;
import com.nri.trading.oms.data.remote.DataClient;
import com.nri.trading.oms.model.OrderEntryRequest;
import com.nri.trading.oms.ui.base.BaseActivity;
import com.nri.trading.oms.ui.main.marketwatch.view.MarketWatchActivity;
import com.nri.trading.oms.ui.main.order.entry.presenter.OrderConfirmationPresenter;
import com.nri.trading.oms.util.DialogFactory;
import com.nri.trading.oms.util.ViewUtil;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import retrofit.RetrofitError;

/**
 * Created by deor on 01-02-2016.
 */
public class OrderEntryConfirmationActivity extends BaseActivity implements OrderConfirmationView{
    @Inject
    OrderConfirmationPresenter mOrderConfirmationPresenter;

    @Bind(R.id.exchange_val)
    TextView exchangeTextView;
    @Bind(R.id.option_type_val)
    TextView optionTypeTextView;
    @Bind(R.id.stock_code_val)
    TextView stockCodeTextView;
    @Bind(R.id.limit_price_val)
    TextView limitPriceTextView;
    @Bind(R.id.quantity_val)
    TextView quantityTextView;
    @Bind(R.id.order_validity_val)
    TextView orderValidityTextView;
    @Bind(R.id.action_val)
    TextView actionTextView;
    @Bind(R.id.order_type_val)
    TextView orderTypeTextView;

    @Inject
    DataClient mDataClient;
    private ProgressDialog progressDialog;
    OrderEntryRequest orderEntryRequest = new OrderEntryRequest();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_entry_confirmation);
        Context context = getApplicationContext();
        activityComponent().inject(this);
        ButterKnife.bind(this);
        mOrderConfirmationPresenter.attachView(this);
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar_order_confirmation);
        setSupportActionBar(mToolbar);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mToolbar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.color_tool_bar_primary)));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        Intent intent = getIntent();
        orderEntryRequest = EventBus.getDefault().removeStickyEvent(OrderEntryRequest.class);
        showOrderEntryData(orderEntryRequest);
    }
    @OnClick(R.id.back)
    public void goBack(){
        onBackPressed();
    }
    @OnClick(R.id.confirm)
    public void submit(){
        mOrderConfirmationPresenter.doConfirm(orderEntryRequest);
        progressDialog = DialogFactory.createProgressDialog(this, R.string.progress_saving);
        progressDialog.show();
        ViewUtil.hideKeyboard(this);
          }

   public void showOrderEntryData(OrderEntryRequest orderEntryRequest){
        exchangeTextView.setText(orderEntryRequest.getExchangeId());
        optionTypeTextView.setText("CALL");
        stockCodeTextView.setText(orderEntryRequest.getSecurityId());
        limitPriceTextView.setText(orderEntryRequest.getPrice().toString());
        quantityTextView.setText(orderEntryRequest.getQuantity().toString());
        orderValidityTextView.setText(orderEntryRequest.getOrderValidity());
        actionTextView.setText(orderEntryRequest.getBuySellType());
        orderTypeTextView.setText(orderEntryRequest.getOrderType());
    }

    @Override public void onResume() {
        super.onResume();

    }

    @Override public void onPause() {
        super.onPause();
    }

    @Override
    public void onSubmitSuccess(boolean status, OrderEntryRequest response) {
        if (status == true) {
            Toast.makeText(OrderEntryConfirmationActivity.this, "Order Placed Successfully",
                    Toast.LENGTH_LONG).show();
            Intent marketWatchIntent = new Intent(OrderEntryConfirmationActivity.this, MarketWatchActivity.class);
            startActivity(marketWatchIntent);
            finish();
        }
        progressDialog.hide();
    }

    @Override
    public void onSubmitFailure(RetrofitError error) {
        progressDialog.hide();
        Toast.makeText(OrderEntryConfirmationActivity.this, "Network Error",
                Toast.LENGTH_LONG).show();
    }
}
