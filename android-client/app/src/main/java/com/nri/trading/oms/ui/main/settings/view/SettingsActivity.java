package com.nri.trading.oms.ui.main.settings.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.google.gson.internal.LinkedTreeMap;
import com.nri.trading.R;
import com.nri.trading.oms.Constants;
import com.nri.trading.oms.OmsApplication;
import com.nri.trading.oms.data.local.PreferencesHelper;
import com.nri.trading.oms.model.InlineResponse200;
import com.nri.trading.oms.ui.base.BaseActivity;
import com.nri.trading.oms.ui.main.marketwatch.view.MarketWatchActivity;
import com.nri.trading.oms.ui.main.settings.presenter.SettingsPresenter;
import com.nri.trading.oms.util.DialogFactory;
import com.nri.trading.oms.util.LinkedHashMapAdapter;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fr.ganfra.materialspinner.MaterialSpinner;

public class SettingsActivity extends BaseActivity implements SettingsView {
    Context context;

    @Inject
    SettingsPresenter settingsPresenter;

    @Bind(R.id.spinner_record_per_page)
    MaterialSpinner mRecordPerPage;
    @Bind(R.id.spinner_locale)
    MaterialSpinner mLocale;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        context = this;
        activityComponent().inject(this);
        settingsPresenter.attachView(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        settingsPresenter.getPrefConstraints();

        ButterKnife.bind(this);

    }

    @OnClick(R.id.btn_pref_save)
    public void savePreferenceClick() {
        DialogFactory.createConfirmDialog(this, R.string.dialog_save_title, R.string.dialog_save_content,
                R.string.dialog_action_cancel, R.string.dialog_action_save, savePreference(), cancelSavePref());

    }

    public Runnable savePreference() {
        return new Runnable() {
            public void run() {
                String selectedLocale = "";
                String selectedRecPerPage = "";
                String prefLanguage = "";
                int localePosition = mLocale.getSelectedItemPosition();
                int recordPerPagePosition = mRecordPerPage.getSelectedItemPosition();
                Map.Entry<String, String> selectedLocaleMap = (Map.Entry<String, String>) mLocale.getAdapter().getItem(localePosition);
                Map.Entry<String, String> selectedRecordPerPageMap = (Map.Entry<String, String>) mRecordPerPage.getAdapter().getItem(recordPerPagePosition);
                selectedLocale = selectedLocaleMap.getKey();
                selectedRecPerPage = selectedRecordPerPageMap.getKey();
                prefLanguage = PreferencesHelper.getSharedPreferenceString(context, Constants.PREF_LOCALE_KEY);
                if (prefLanguage != null) {
                    if (!prefLanguage.equalsIgnoreCase(selectedLocale)) {
                        ((OmsApplication) getApplicationContext()).changeLang(selectedLocale);
                        restartActivity();
                    }
                } else {
                    ((OmsApplication) getApplicationContext()).changeLang(selectedLocale);
                    restartActivity();
                }

                PreferencesHelper.setSharedPreferenceString(context, Constants.PREF_LOCALE_KEY, selectedLocale);
                PreferencesHelper.setSharedPreferenceString(context, Constants.PREF_RECORD_PER_PAGE_KEY, selectedRecPerPage);
                PreferencesHelper.setSharedPreferenceString(context, Constants.PREF_LOCALE_POS_KEY, String.valueOf(localePosition));
                PreferencesHelper.setSharedPreferenceString(context, Constants.PREF_RECORD_PER_PAGE_POS_KEY, String.valueOf(recordPerPagePosition));
                Toast.makeText(context, R.string.pref_save_success, Toast.LENGTH_LONG).show();
            }
        };
    }

    public Runnable cancelSavePref() {
        return new Runnable() {
            public void run() {

            }
        };
    }


    @Override
    public void populatePrefData(InlineResponse200 inlineResponse200) {

        for (Map.Entry<String, Object> entry : inlineResponse200.entrySet()) {
            if (entry.getKey().equalsIgnoreCase(Constants.PREF_LOCALE_KEY)) {
                populateLocaleSpinner((LinkedTreeMap<String, String>) entry.getValue());
            }
            if (entry.getKey().equalsIgnoreCase(Constants.PREF_RECORD_PER_PAGE_KEY)) {
                populateRecordPerPageSpinner((LinkedTreeMap<String, String>) entry.getValue());
            }
        }

    }

    void populateLocaleSpinner(LinkedTreeMap<String, String> map) {
        String selectedValue = PreferencesHelper.getSharedPreferenceString(context, Constants.PREF_LOCALE_KEY);

        LinkedHashMap localeList = new LinkedHashMap<>();

        for (Map.Entry<String, String> val : map.entrySet()) {
            int resourceId = getResources().getIdentifier(val.getValue(), "string", getPackageName());
            localeList.put(val.getKey(), getResources().getString(resourceId));

        }
        LinkedHashMapAdapter<String, String> arrayAdapter = new LinkedHashMapAdapter<>(this, R.layout.spinner_layout, localeList);
        arrayAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        mLocale.setAdapter(arrayAdapter);
        String localPosSelected = PreferencesHelper.getSharedPreferenceString(this, Constants.PREF_LOCALE_POS_KEY);
        if(localPosSelected!=null){
            mLocale.setSelection(Integer.parseInt(localPosSelected));
        }
    }

    void populateRecordPerPageSpinner(LinkedTreeMap<String, String> map) {
        LinkedHashMap recordPerPageList = new LinkedHashMap<>();

        for (Map.Entry<String, String> val : map.entrySet()) {
            int resourceId = getResources().getIdentifier(val.getValue(), "string", getPackageName());
            recordPerPageList.put(val.getKey(), getResources().getString(resourceId));
        }
        LinkedHashMapAdapter<String, String> arrayAdapter = new LinkedHashMapAdapter<>(this, R.layout.spinner_layout, recordPerPageList);
        arrayAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        mRecordPerPage.setAdapter(arrayAdapter);
        String recPerPagePosSelected = PreferencesHelper.getSharedPreferenceString(this, Constants.PREF_RECORD_PER_PAGE_POS_KEY);
        if(recPerPagePosSelected !=null){
            mRecordPerPage.setSelection(Integer.parseInt(recPerPagePosSelected));
        }
    }

    void restartActivity() {
        Intent marketWatchWatchIntent = new Intent(SettingsActivity.this, MarketWatchActivity.class);
        marketWatchWatchIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(marketWatchWatchIntent);
    }


}
