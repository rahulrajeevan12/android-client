package com.nri.trading.oms.ui.main.order.entry.presenter;

import com.nri.trading.oms.api.OrdercontrollerApi;
import com.nri.trading.oms.data.remote.DataClient;
import com.nri.trading.oms.model.OrderEntryRequest;
import com.nri.trading.oms.ui.base.Presenter;
import com.nri.trading.oms.ui.main.order.entry.view.OrderConfirmationView;

import javax.inject.Inject;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by deor on 05-02-2016.
 */
public class OrderConfirmationPresenter implements Presenter<OrderConfirmationView> {

    OrderConfirmationView orderConfirmationView;
    @Inject
    DataClient mDataClient;

    @Inject
    public OrderConfirmationPresenter() {
    }

    public void doConfirm(OrderEntryRequest orderEntryRequest){
        OrdercontrollerApi ordercontrollerApi = mDataClient.createService(OrdercontrollerApi.class);
        ordercontrollerApi.submitEntryUsingPOST(orderEntryRequest, new Callback<OrderEntryRequest>() {
            @Override
            public void success(OrderEntryRequest orderEntryRequest, Response response) {
                orderConfirmationView.onSubmitSuccess(true,orderEntryRequest);
            }
            @Override
            public void failure(RetrofitError error) {
                orderConfirmationView.onSubmitFailure(error);
            }
        });
    }
    @Override
    public void detachView() {
        this.orderConfirmationView = null;
    }
    @Override
    public void attachView(OrderConfirmationView orderConfirmationView) {
        this.orderConfirmationView = orderConfirmationView;
    }
}
