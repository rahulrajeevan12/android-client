/*
 * +=======================================================================+
 * |                                                                       |
 * |          Copyright (C) 2015-16 Nomura Research Institute Ltd          |
 * |                          All Rights Reserved                          |
 * |                                                                       |
 * |    This document is the sole property of Nomura Research Institute    |
 * |    Ltd. No part of this document may be reproduced in any form or     |
 * |    by any means - electronic, mechanical, photocopying, recording     |
 * |    or otherwise - without the prior written permission of Nomura      |
 * |    Research Institute Ltd                                             |
 * |                                                                       |
 * |    Unless required by applicable law or agreed to in writing,         |
 * |    software distributed under the License is distributed on an        |
 * |    "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,       |
 * |    either express or implied.                                         |
 * |                                                                       |
 * +=======================================================================+
 */
package com.nri.trading.oms.ui.main.marketwatch.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.nri.trading.oms.ui.main.marketwatch.view.MarketWatchTabFragment;

import java.util.HashMap;

/**
 * Created by rahulc on 19.1.16.
 */
public class MarketWatchPagerAdapter extends FragmentPagerAdapter {
    Context context;

    public MarketWatchPagerAdapter(FragmentManager fm,Context context) {
        super(fm);
        this.context = context;

    }


    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position) {
            case 0:

                fragment = (fragment == null ) ?  new MarketWatchTabFragment(getPageTitle(0).toString()):fragment;
                break;
            case 1:
                fragment = (fragment == null ) ?  new MarketWatchTabFragment(getPageTitle(1).toString()):fragment;
                break;
            case 2:
                fragment = (fragment == null ) ?  new MarketWatchTabFragment(getPageTitle(2).toString()):fragment;
                break;
                   }
        return fragment;
    }


    @Override
    public int getCount() {

        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Equity Option";
            case 1:
                return "Index Future";
            case 2:
                return "Index Option";
        }
        return null;
    }
}
