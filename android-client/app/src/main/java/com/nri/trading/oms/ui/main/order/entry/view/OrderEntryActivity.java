package com.nri.trading.oms.ui.main.order.entry.view;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.nri.trading.R;
import com.nri.trading.oms.data.remote.DataClient;
import com.nri.trading.oms.model.InstrumentSummary;
import com.nri.trading.oms.model.OrderEntryRequest;
import com.nri.trading.oms.model.ValidationResponse;
import com.nri.trading.oms.ui.base.BaseActivity;
import com.nri.trading.oms.ui.main.order.entry.presenter.OrderEntryPresenter;
import com.nri.trading.oms.util.DialogFactory;
import com.nri.trading.oms.util.ViewUtil;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import retrofit.RetrofitError;

/**
 * Created by deor on 18-01-2016.
 */
public class OrderEntryActivity extends BaseActivity implements OrderEntryView{

    @Inject
    OrderEntryPresenter mOrderEntryPresenter;

    @Bind(R.id.trading_power_text)
    TextView tradingPowerText;
    @Bind(R.id.exchanges)
    TextView exchangeText;
    @Bind(R.id.option_types)
    TextView optionTypeText;
    @Bind(R.id.stock_codes)
    TextView stockCodeText;
    @Bind(R.id.contract_details_edit_text)
    TextView contractDetailsText;
    @Bind(R.id.action_group)
    RadioGroup actionRadioGroup;
    @Bind(R.id.order_type_group)
    RadioGroup orderTypeRadioGroup;
    @Bind(R.id.order_validity_group)
    RadioGroup orderValidityRadioGroup;
    @Bind(R.id.quantity_edit_text)
    EditText quantityEditText;
    @Bind(R.id.limit_price_edit_text)
    EditText limitPriceEditText;

    @Inject
    DataClient mDataClient;
    OrderEntryRequest orderEntryRequest = new OrderEntryRequest();
    InstrumentSummary instrumentSummary = new InstrumentSummary();
    private ProgressDialog mProgressDialog;
    boolean flag = true;

    //private EventBus bus = EventBus.getDefault();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_entry);
        Context context = getApplicationContext();
        activityComponent().inject(this);
        ButterKnife.bind(this);
        mOrderEntryPresenter.attachView(this);
        //bus.register(this);
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mToolbar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.color_tool_bar_primary)));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        Intent intent = getIntent();
        instrumentSummary = EventBus.getDefault().removeStickyEvent(InstrumentSummary.class);
        tradingPowerText.setText("1000");
        exchangeText.setText("ASX");
        optionTypeText.setText((instrumentSummary.getOptionType()== null)?"CALL" :(instrumentSummary.getOptionType().equals("P")?"PUT":"CALL") );
        stockCodeText.setText(instrumentSummary.getSecurityCode());
        contractDetailsText.setText("Details About Contract");
       // quantityEditText.setText("100");


      //  limitPriceEditText.setText("1200");
        RadioButton rdss = (RadioButton) orderTypeRadioGroup.getChildAt(0);
        rdss.setChecked(true);
        orderTypeRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                RadioButton rb = (RadioButton) findViewById(checkedId);
                Toast.makeText(getApplicationContext(), rb.getText(), Toast.LENGTH_SHORT).show();
            }
        });

        RadioButton rds = (RadioButton) orderValidityRadioGroup.getChildAt(0);
        rds.setChecked(true);
        orderValidityRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                RadioButton rb = (RadioButton) findViewById(checkedId);
                Toast.makeText(getApplicationContext(), rb.getText(), Toast.LENGTH_SHORT).show();
            }
        });



        RadioButton rd = (RadioButton) actionRadioGroup.findViewById(getResources().getIdentifier(intent.getStringExtra("BuySellType").toString(),"id",getPackageName()));
        rd.setChecked(true);


    }

    @OnClick(R.id.save)
    public void validate() {
        String quantity = quantityEditText.getText().toString();
        flag = true;
        if(TextUtils.isEmpty(quantity)) {
                flag = false;
            Toast.makeText(OrderEntryActivity.this, "Please Enter Quantity",
                    Toast.LENGTH_LONG).show();
            }
        if (flag) {
            orderEntryRequest.setBuySellType("B");
            orderEntryRequest.setExchangeId(exchangeText.getText().toString());
            orderEntryRequest.setOrderType("Market");
            orderEntryRequest.setOrderValidity("Day");
            orderEntryRequest.setQuantity(Double.valueOf(quantityEditText.getText().toString()));
            orderEntryRequest.setSecurityId(instrumentSummary.getSecurityCode());
            orderEntryRequest.setPrice(1200d);
            mOrderEntryPresenter.doValidate(orderEntryRequest);
            mProgressDialog = DialogFactory.createProgressDialog(this, R.string.progress_validating);
            mProgressDialog.show();
            ViewUtil.hideKeyboard(this);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        // bus.unregister(this);
    }


    @Override
    public void populateOrderEntry() {

    }
    @Override
    public void onValidateSuccess(boolean status, ValidationResponse response) {
        if (status == true) {
            Intent orderConfirmationIntent = new Intent(OrderEntryActivity.this, OrderEntryConfirmationActivity.class);
            de.greenrobot.event.EventBus.getDefault().postSticky(orderEntryRequest);
            // orderConfirmationIntent.putExtra("orderEntryData",populateOrderEntryData(orderEntryData));
            //  BusProvider.getInstance().post(orderEntryRequest);
            startActivity(orderConfirmationIntent);
            overridePendingTransition(R.anim.activity_left_slide, R.anim.activity_right_slide);
        }

    }

    @Override
    public void onValidateFailure(RetrofitError error) {
        mProgressDialog.hide();
        Toast.makeText(OrderEntryActivity.this, "Validation Error",
                Toast.LENGTH_LONG).show();
    }
}
