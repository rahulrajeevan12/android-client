/*
 * +=======================================================================+
 * |                                                                       |
 * |          Copyright (C) 2015-16 Nomura Research Institute Ltd          |
 * |                          All Rights Reserved                          |
 * |                                                                       |
 * |    This document is the sole property of Nomura Research Institute    |
 * |    Ltd. No part of this document may be reproduced in any form or     |
 * |    by any means - electronic, mechanical, photocopying, recording     |
 * |    or otherwise - without the prior written permission of Nomura      |
 * |    Research Institute Ltd                                             |
 * |                                                                       |
 * |    Unless required by applicable law or agreed to in writing,         |
 * |    software distributed under the License is distributed on an        |
 * |    "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,       |
 * |    either express or implied.                                         |
 * |                                                                       |
 * +=======================================================================+
 */
package com.nri.trading.oms.ui.main.marketwatch.presenter;

import android.util.Log;

import com.nri.trading.oms.api.RefdatacontrollerApi;
import com.nri.trading.oms.data.remote.DataClient;
import com.nri.trading.oms.model.InlineResponse200;
import com.nri.trading.oms.model.InstrumentMktDataDTO;
import com.nri.trading.oms.ui.base.Presenter;
import com.nri.trading.oms.ui.main.marketwatch.view.MarketWatchView;

import java.util.List;

import javax.inject.Inject;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by rahulc on 20.1.16.
 */
public class MarketWatchPresenter implements Presenter<MarketWatchView> {

    MarketWatchView marketWatchView;
    @Inject
    DataClient mDataClient;

    @Inject
    public MarketWatchPresenter() {
    }

    public void getMarket(){
        RefdatacontrollerApi refService = mDataClient.createService(RefdatacontrollerApi.class);
        refService.getMarketsUsingGET(new Callback<InlineResponse200>() {
            @Override
            public void success(InlineResponse200 inlineResponse200, Response response) {
                marketWatchView.populateMarketSpinner(inlineResponse200);
                Log.d("success: ", inlineResponse200.toString());
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d("success: ", error.toString());
            }
        });
    }


    @Override
    public void attachView(MarketWatchView mvpView) {
        this.marketWatchView = mvpView;
    }

    @Override
    public void detachView() {
        this.marketWatchView = null;
    }

}
