/*
 * +=======================================================================+
 * |                                                                       |
 * |          Copyright (C) 2015-16 Nomura Research Institute Ltd          |
 * |                          All Rights Reserved                          |
 * |                                                                       |
 * |    This document is the sole property of Nomura Research Institute    |
 * |    Ltd. No part of this document may be reproduced in any form or     |
 * |    by any means - electronic, mechanical, photocopying, recording     |
 * |    or otherwise - without the prior written permission of Nomura      |
 * |    Research Institute Ltd                                             |
 * |                                                                       |
 * |    Unless required by applicable law or agreed to in writing,         |
 * |    software distributed under the License is distributed on an        |
 * |    "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,       |
 * |    either express or implied.                                         |
 * |                                                                       |
 * +=======================================================================+
 */
package com.nri.trading.oms.ui.main.marketwatch.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.nri.trading.R;
import com.nri.trading.oms.model.InstrumentSummary;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by rahulc on 19.1.16.
 */
public class MarketWatchListAdapter extends BaseAdapter implements Filterable {

    private LayoutInflater inflater;
    List<InstrumentSummary> marketWatchDataList = new ArrayList<>();
    List<InstrumentSummary> mDisplayedValues;
    Context context;
    private int count;
    private int stepNumber;
    private int startCount;

    public MarketWatchListAdapter(Context context, List<InstrumentSummary> marketWatchMainData, int startCount, int stepNumber) {
        this.mDisplayedValues = marketWatchMainData;
        this.context = context;
        this.marketWatchDataList = marketWatchMainData;
        inflater = LayoutInflater.from(this.context);
        this.startCount = Math.min(startCount, marketWatchDataList.size()); //don't try to show more views than we have
        this.count = this.startCount;
        this.stepNumber = stepNumber;

    }

    @Override
    public int getCount() {
        return mDisplayedValues.size();
    }

    @Override
    public InstrumentSummary getItem(int i) {
        return mDisplayedValues.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, final ViewGroup viewGroup) {
        final MarketWatchViewHolder marketWatchViewHolder;

        if (view == null) {
            view = inflater.inflate(R.layout.market_watch_main_list, viewGroup, false);
            marketWatchViewHolder = new MarketWatchViewHolder(view);
            view.setTag(marketWatchViewHolder);
        } else {
            marketWatchViewHolder = (MarketWatchViewHolder) view.getTag();
        }

        final InstrumentSummary currentData = getItem(i);

        marketWatchViewHolder.companyName.setText(currentData.getSecurityName());
        marketWatchViewHolder.companyShortName.setText(currentData.getSecurityCode());
        //TODO remove hardcode values
        if(currentData.getLastPrice() == null)currentData.setLastPrice(50.24d);
        if(currentData.getBasePrice() == null)currentData.setBasePrice(50.01d);
        marketWatchViewHolder.headValue.setText(currentData.getLastPrice().toString());
        DecimalFormat f = new DecimalFormat("##.000");
        double changeVal =Double.valueOf(f.format(currentData.getLastPrice() - currentData.getBasePrice()));
        double percentVal = Double.valueOf(f.format((changeVal)/100));
        double lValue = percentVal;
        String lv = lValue + "%";
        marketWatchViewHolder.lValue.setText(lv);
        if (lValue > 0) {
            marketWatchViewHolder.lValue.setTextColor(Color.parseColor("#2e992e"));
        } else {
            marketWatchViewHolder.lValue.setTextColor(Color.parseColor("#dc2323"));
        }
        double rValue = changeVal;
        String rv;

        if (rValue > 0) {
            marketWatchViewHolder.rValue.setTextColor(Color.parseColor("#2e992e"));
            rv = "+" + rValue;
        } else {
            marketWatchViewHolder.rValue.setTextColor(Color.parseColor("#dc2323"));
            rv = String.valueOf(rValue);
        }
        marketWatchViewHolder.rValue.setText(rv);
        return view;
    }
    /**
     * Show more views, or the bottom
     * @return true if the entire data set is being displayed, false otherwise
     */
    public boolean showMore(){
        if(count == marketWatchDataList.size()) {
            return true;
        }else{
            count = Math.min(count + stepNumber, marketWatchDataList.size()); //don't go past the end
            notifyDataSetChanged(); //the count size has changed, so notify the super of the change
            return endReached();
        }
    }
    /**
     * @return true if then entire data set is being displayed, false otherwise
     */
    public boolean endReached(){
        return count == marketWatchDataList.size();
    }

    /**
     * Sets the ListView back to its initial count number
     */
    public void reset(){
        count = startCount;
        notifyDataSetChanged();
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                FilterResults results = new FilterResults();
                ArrayList<InstrumentSummary> filteredArrList = new ArrayList<InstrumentSummary>();
                if (marketWatchDataList == null) {
                    marketWatchDataList = new ArrayList<InstrumentSummary>(mDisplayedValues);
                }
                if (charSequence == null || charSequence.length() == 0) {
                    results.count = marketWatchDataList.size();
                    results.values = marketWatchDataList;
                } else {
                    charSequence = charSequence.toString().toLowerCase();
                    for (int i = 0; i < marketWatchDataList.size(); i++) {
                        String data = marketWatchDataList.get(i).getSecurityName();
                        if (data.toLowerCase().startsWith(charSequence.toString())) {
                            InstrumentSummary marketWatchMainData = new InstrumentSummary();
                            marketWatchMainData.setSecurityName(marketWatchDataList.get(i).getSecurityName());
                            marketWatchMainData.setSecurityCode(marketWatchDataList.get(i).getSecurityCode());
                            marketWatchMainData.setLastPrice(marketWatchDataList.get(i).getLastPrice());

                          //  marketWatchMainData.setHeadLeftValue(marketWatchDataList.get(i).get());
                          //  marketWatchMainData.setHeadRightValue(marketWatchDataList.get(i).getHeadRightValue());
                            filteredArrList.add(marketWatchMainData);
                        }
                    }
                    // set the Filtered result to return
                    results.count = filteredArrList.size();
                    results.values = filteredArrList;
                }
                return results;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mDisplayedValues = (ArrayList<InstrumentSummary>) filterResults.values;
                notifyDataSetChanged();
            }
        };
        return filter;
    }

    private class MarketWatchViewHolder {
        TextView companyName;
        TextView companyShortName;
        TextView headValue;
        TextView lValue;
        TextView rValue;

        public MarketWatchViewHolder(View view) {
            companyName = (TextView) view.findViewById(R.id.companyName);
            companyShortName = (TextView) view.findViewById(R.id.companyShortName);
            headValue = (TextView) view.findViewById(R.id.headValue);
            lValue = (TextView) view.findViewById(R.id.headLeftValue);
            rValue = (TextView) view.findViewById(R.id.headRightValue);
        }

    }
}
