/*
 * +=======================================================================+
 * |                                                                       |
 * |          Copyright (C) 2015-16 Nomura Research Institute Ltd          |
 * |                          All Rights Reserved                          |
 * |                                                                       |
 * |    This document is the sole property of Nomura Research Institute    |
 * |    Ltd. No part of this document may be reproduced in any form or     |
 * |    by any means - electronic, mechanical, photocopying, recording     |
 * |    or otherwise - without the prior written permission of Nomura      |
 * |    Research Institute Ltd                                             |
 * |                                                                       |
 * |    Unless required by applicable law or agreed to in writing,         |
 * |    software distributed under the License is distributed on an        |
 * |    "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,       |
 * |    either express or implied.                                         |
 * |                                                                       |
 * +=======================================================================+
 */
package com.nri.trading.oms.ui.main.marketwatch.presenter;

import com.nri.trading.oms.api.RefdatacontrollerApi;
import com.nri.trading.oms.data.remote.DataClient;
import com.nri.trading.oms.model.InstrumentMktDataDTO;
import com.nri.trading.oms.ui.base.Presenter;
import com.nri.trading.oms.ui.main.marketwatch.view.MarketWatchDetailsView;

import javax.inject.Inject;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by rahulc on 20.1.16.
 */
public class MarketWatchDetailsPresenter implements Presenter<MarketWatchDetailsView> {
    MarketWatchDetailsView marketWatchDetailsView;

    @Inject
    DataClient mDataClient;

    @Inject
    public MarketWatchDetailsPresenter() {
    }


    @Override
    public void attachView(MarketWatchDetailsView mvpView) {
        this.marketWatchDetailsView = mvpView;
    }

    @Override
    public void detachView() {
        this.marketWatchDetailsView = null;
    }

    public void getMktDataForSecurity(String marketPk, String instrumentPk) {
        /*TODO be removed*/
        if (true) {
            Double dbl = 987.0;
            InstrumentMktDataDTO instrumentMktDataDTO = new InstrumentMktDataDTO();
            instrumentMktDataDTO.setLastPrice(dbl);
            instrumentMktDataDTO.setBasePrice(dbl);
            instrumentMktDataDTO.setAskPrice(dbl);
            instrumentMktDataDTO.setBidPrice(dbl);
            instrumentMktDataDTO.setHighPrice(dbl);
            instrumentMktDataDTO.setHighPriceLimit(dbl);
            instrumentMktDataDTO.setHighPrice(dbl);
            instrumentMktDataDTO.setLowPrice52wk(dbl);
            instrumentMktDataDTO.setOpenPrice(dbl);
            instrumentMktDataDTO.setLowPrice(dbl);
            instrumentMktDataDTO.setNoOfAsks(dbl);
            instrumentMktDataDTO.setNoOfBids(dbl);
            instrumentMktDataDTO.setValue(dbl);
            instrumentMktDataDTO.setVolume(dbl);
            marketWatchDetailsView.populateBottomSheet(instrumentMktDataDTO);
            return;
        }
        RefdatacontrollerApi refdatacontrollerApi = mDataClient.createService(RefdatacontrollerApi.class);
        refdatacontrollerApi.getMktDataForSecurityUsingGET(marketPk, instrumentPk, new Callback<InstrumentMktDataDTO>() {
            @Override
            public void success(InstrumentMktDataDTO instrumentMktDataDTO, Response response) {
                marketWatchDetailsView.populateBottomSheet(instrumentMktDataDTO);
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

}
