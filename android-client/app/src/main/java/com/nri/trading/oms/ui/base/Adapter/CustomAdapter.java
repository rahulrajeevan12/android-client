package com.nri.trading.oms.ui.base.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.flipboard.bottomsheet.BottomSheetLayout;
import com.nri.trading.R;
import com.nri.trading.oms.data.model.MarketWatchDetailsData;
import com.nri.trading.oms.data.model.StickViewDetailItem;
import com.nri.trading.oms.ui.main.marketwatch.adapter.StockViewAdapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by deor on 29-01-2016.
 */
public class CustomAdapter<T> extends ArrayAdapter<T> {
    private LayoutInflater layoutInflater;

    /**
     * {@inheritDoc}
     *
     * @param position
     */
    @Override
    public T getItem(int position) {
        return super.getItem(position);
    }

    ViewGroup viewGroup;
    List<T> list = Collections.emptyList();
    Context context;

    public CustomAdapter(Context context,int viewResourceId, List<T>list) {
        super(context,viewResourceId,list);
        layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.list = list;
    }
    static class ViewHolderItem {
        public ViewHolderItem(View itemView){

        }
    }

    }


