package com.nri.trading.oms.ui.main.navigation.presenter;

import android.util.Log;

import com.nri.trading.oms.api.UicontrollerApi;
import com.nri.trading.oms.data.remote.DataClient;
import com.nri.trading.oms.model.MenuDTO;
import com.nri.trading.oms.ui.base.Presenter;
import com.nri.trading.oms.ui.main.navigation.view.NavigationView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by deor on 12-01-2016.
 */

public class NavigationPresenter implements Presenter<NavigationView> {
    NavigationView navigationView;
    @Inject
    DataClient mDataClient;

    @Inject
    public NavigationPresenter() {

    }

    public void getMenuList() {
        /*TODO remove all*/
        MenuDTO menuDTO = new MenuDTO();
        menuDTO.setId("1");
        menuDTO.setNameKey("trading.oms.menu.order");
        menuDTO.setStyleClass("");
        menuDTO.setUrl("");

        List<MenuDTO> menuDTOs = new ArrayList<>();
        menuDTOs.add(menuDTO);
        menuDTO = new MenuDTO();
        menuDTO.setId("2");
        menuDTO.setNameKey("trading.oms.menu.orderentry");
        menuDTO.setStyleClass("");
        menuDTO.setUrl("");
        menuDTOs.add(menuDTO);

        menuDTO = new MenuDTO();
        menuDTO.setId("3");
        menuDTO.setNameKey("trading.oms.menu.ordersummary");
        menuDTO.setStyleClass("");
        menuDTO.setUrl("");
        menuDTOs.add(menuDTO);
        navigationView.populateMenu(menuDTOs);
        /*TODO remove all*/
        UicontrollerApi authService = mDataClient.createService(UicontrollerApi.class);
        authService.getMenuUsingGET("", new Callback<List<MenuDTO>>() {
            @Override
            public void success(List<MenuDTO> menuDTOs, Response response) {
                navigationView.populateMenu(menuDTOs);
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("Error loading  Menu", error.toString());
            }
        });
    }

    @Override
    public void attachView(NavigationView mvpView) {
        this.navigationView = mvpView;
    }

    @Override
    public void detachView() {
        this.navigationView = null;
    }
}
