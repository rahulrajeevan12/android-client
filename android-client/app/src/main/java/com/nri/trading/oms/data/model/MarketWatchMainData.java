package com.nri.trading.oms.data.model;

/**
 * Created by rahulc on 19.1.16.
 */

public class MarketWatchMainData {
    String companyName;
    String companyShortName;
    String headValue;
    String headRightValue;
    String headLeftValue;

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyShortName() {
        return companyShortName;
    }

    public void setCompanyShortName(String companyShortName) {
        this.companyShortName = companyShortName;
    }

    public String getHeadValue() {
        return headValue;
    }

    public void setHeadValue(String headValue) {
        this.headValue = headValue;
    }

    public String getHeadRightValue() {
        return headRightValue;
    }

    public void setHeadRightValue(String headRightValue) {
        this.headRightValue = headRightValue;
    }

    public String getHeadLeftValue() {
        return headLeftValue;
    }

    public void setHeadLeftValue(String headLeftValue) {
        this.headLeftValue = headLeftValue;
    }
}
